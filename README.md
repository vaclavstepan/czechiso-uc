# czechiso-uc

We modified directly the BST code, so I'm not including the DBJ and MBS.
You can find these at [David Martinek's page][http://www.fit.vutbr.cz/~martinek/latex/czechiso.html]

CSN ISO 690 BibTeX bibliography style modified as follows:

 - author names are in upper case.
 - DOI links are clickable (modified by Martin Sefl, martin.sefl@gmail.com)

